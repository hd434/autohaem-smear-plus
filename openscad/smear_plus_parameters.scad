include <../autohaem-smear/openscad/shared_parameters.scad>;

version_numstring ="v2.0.0";


//Main box
radius_of_curvature = 1;
box_size = [110,50,40];

// BASE
lid_thickness=5;
wall_thickness = 3;
base_size = [105,75, box_size.z-lid_thickness];
base_screw_offset = 5;
arduino_dimensions = [54, 69, 27];
stepper_dimensions = [44, 44, 27];
arduino_position = [15,10,0];
stepper_position = [66,15,0];
rod_height = 8;

slider_angle = 50;


//Rod
rod_radius = 3;
rod_length=100;
rod_radius_slide_bodge = 0.25;
rod_radius_bodge = 0.05;
distance_between_rods = box_size.y-10;

rod_holder_size = [10,10,17];


//Motor
nema_17_size = [39.5,42.3,42.3];
vibration_dampener_depth = 6;

//Limit switch
limit_switch_size=[15,14,6];
limit_switch_wall_thickness=1;
limit_switch_lid_thickness=5;
limit_switch_bodge_factor=[1,1,1];
limit_switch_offset = [microscope_slide_offset+microscope_slide_size.x+microscope_slide_bodge_factor+limit_switch_bodge_factor.x+5,(microscope_slide_size.y+microscope_slide_bodge_factor)-2*5.7-limit_switch_size.y-2*limit_switch_bodge_factor.y+19.5,0];



# Assemble Lid

{{BOM}}

[lid]: models/smear_plus_main_body.stl "{cat:3DPrinted}"
[8mm Diameter LED Push Button]: components/LED_button.md "{cat:part}"
[M2x10mm screw]: "{cat:part}"
[M3x8mm screw]: "{cat:part}"
[M3 nut]: "{cat:part}"
[limit switch lid]: models/limit_switch_lid.stl "{cat:3DPrinted}"
[M2 hex screwdriver]: "{cat:tool}"
[M2.5 hex screwdriver]: "{cat:tool}"
[100mm ground steel rod]: components/rod.md "{cat:part}"

# Method

## Attach the LED pushbutton onto the lid {pagestep}

Push the bottom of the [soldered LED pushbutton](fromstep){qty:1} through the circular hole in the corner of the [lid]{qty:1}.

![](images/assemble_lid/mount_led_1.jpg)
![](images/assemble_lid/mount_led_2.jpg)

## Attach the sensor switch onto the lid {pagestep}

Place the [soldered limit switch](fromstep){qty:1} in position on the lid. Screw the sensor switch and the lid together using two [M2x10mm screw]{qty:2}s with a [M2 hex screwdriver]{qty:1}.

![](images/assemble_lid/mount_sensor.jpg)

## Place the sensor switch lid on top of the sensor {pagestep}

Attach the [limit switch lid]{qty:1} onto the base using two [M3x8mm screw]{qty:2}s with a [M2.5 hex screwdriver]{qty:1}

![](images/assemble_lid/mount_sensor_lid.jpg)

## Secure the rod into place {pagestep}

Insert two [M3 nut]{qty:2}s into the nut traps of the rod holders using a [M2.5 hex screwdriver]{qty:1}.

Insert the [100mm ground steel rod]{qty:1} through the rod holders and secure it using two [M3x8mm screw]{qty:2}s.

![](images/assemble_lid/mount_rod.jpg)

You have now successfully built the [assembled lid]{output, Qty:1}. You will need this later.

use <../autohaem-smear/openscad/utilities.scad>
include <./smear_plus_parameters.scad>;


// limit_switch Case and Lid

module each_limit_switch_mount(){
    translate([limit_switch_size.x/2+limit_switch_wall_thickness,-3.5,0])repeat([0,(limit_switch_size+limit_switch_bodge_factor).y+2*limit_switch_wall_thickness+2*3.5,0],2)children();
}

module each_2mm_holes(){
    translate([5.5,4.5,0])repeat([0,6.5,0],2)children();
}

module limit_switch_mounts(){
    each_limit_switch_mount(){
        cylinder(r=3.5,h=rod_height+rod_radius_slide_bodge/2+limit_switch_size.z/2,$fn=100);
    }
}

module limit_switch_lid_mounts(){
    each_limit_switch_mount(){
        cylinder(r=3.5,h=limit_switch_lid_thickness,$fn=100);
    }
}

module limit_switch_mount_holes(){
    translate([0,0,limit_switch_size.z+limit_switch_bodge_factor.z-6.5])each_limit_switch_mount(){
        trylinder_selftap(nominal_d=3,h=100,$fn=100);
    }
}

module limit_switch_2mm_holes(){
    translate([0,0,-10])each_2mm_holes(){
        trylinder_selftap(nominal_d=1.5,h=1000,$fn=100);
    }
}

module limit_switch_2mm_indents(){
    each_2mm_holes(){
        cylinder(r=2,h=2,$fn=100);
    }
}

module limit_switch_box(){ 
    translate(limit_switch_offset+[0,0,lid_thickness]){
        difference(){
            hull(){
                minkowski(){
                    cube([limit_switch_size.x+limit_switch_bodge_factor.x+2*limit_switch_wall_thickness,limit_switch_size.y+limit_switch_bodge_factor.y+2*limit_switch_wall_thickness,rod_height+rod_radius_slide_bodge/2+limit_switch_size.z/2]);
                    cylinder(r=radius_of_curvature,h = tiny(),$fn=100);
                    }
                limit_switch_mounts();
            }
            limit_switch_mount_holes();
            translate([-10,limit_switch_wall_thickness,rod_height+rod_radius_slide_bodge/2-limit_switch_size.z/2]){                        
                minkowski(){
                    cube(limit_switch_size+limit_switch_bodge_factor+[10,0,10]);
                    cylinder(r=radius_of_curvature,h = tiny(),$fn=100);
                }
            }
        }           
    }
}

module limit_switch_lid(){
    translate([0,-25,0]){
        difference(){
            hull(){
                    minkowski(){
                        cube([(limit_switch_size+limit_switch_bodge_factor).x,(limit_switch_size+limit_switch_bodge_factor).y,limit_switch_lid_thickness]+[2*limit_switch_wall_thickness,2*limit_switch_wall_thickness,0]);
                        cylinder(r=radius_of_curvature,h = tiny(),$fn=100);
                        }
                    limit_switch_lid_mounts();
                }
            each_limit_switch_mount(){
                translate([0,0,limit_switch_lid_thickness-2.9])cylinder(d=5.7,h=3,$fn=100);
                cylinder(d=3,h=100,$fn=100);
            }
            translate([0,1,0])each_2mm_holes(){
                cylinder(d=4.5,h=2.5,$fn=100);
            }
        }
    }
    
    
}

limit_switch_lid();

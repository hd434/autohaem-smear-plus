# Motor power supply

* 24W power supply
* 12V
* 2A
* 2.1mm jack

Available from e.g. [RS 121-7157](https://uk.rs-online.com/web/p/ac-dc-adapters/1217157/)


# Assemble Slider

{{BOM}}

[M3x8mm screw]: "cat:part"
[2.5x7x2.5mm N42 Neodymium magnet]: components/magnets.md"{cat:part}"
[lead screw nut]: "cat:part"

[bridge]: components/slider_motor.md#bridge "{cat:3DPrinted}"
[slider]: components/slider_motor.md#slider "{cat:3DPrinted}"
[glue]: "{cat:part}"
[M2.5 hex screwdriver]: "{cat:tool}"

# Method

## Assembling the slider {pagestep}

Attach the [bridge]{qty:1} (?) onto the [slider]{qty:1} using two [M3x8mm screw]{qty:2}s with a [M2.5 hex screwdriver]{qty:1}.

![](images/attach_slider/attach_bridge.jpg)

## Glue the magnet onto the slider {pagestep}

Use some [glue]{qty: some} to attach the [2.5x7x2.5mm N42 Neodymium magnet]{qty:1} into the cavity of the [bridge].

![](images/attach_slider/magnet.jpg)

## Attach the limit switch screw {pagestep}

Screw in an [M3x8mm screw]{qty:1} at the back of the slider with a [M2.5 hex screwdrover] such that it is poking out slightly (by about 3mm). This will be used to push the limit switch during calibration of the instrument.

![](images/attach_slider/slider_screw.jpg)

You have now successfully built the [assembled slider]{output, Qty:1}. You will need this later.

# Attach Everything Together
{{BOM}}

[8mm Diameter LED Push Button]: components/LED_button.md "{cat:part}"
[M2.5mm screws]: "{cat:part}"
[M3x6mm screws]: "{cat:part}"
[lead screw nut]: components/nema_motor.md#nut "{cat:part}"
[8mm Diameter LED Pushbutton]: components/LED_button.md "{cat:part}"
[M2.5 hex screwdriver]: "{cat:tool}"
[Arduino power supply]: components/arduino_power_supply.md "{cat:part}"
[Motor power supply]: components/motor_power_supply.md "{cat:part}"
[Arduino Uno]: components/Arduino_uno.md "{cat:part}"
[42CH Stepper Motor Driver Expansion Board]: components/expansion_board.md "{cat:part}"

# Method
## Connect the electronics {pagestep}

Connect the wires from the LED pushbutton and the sensor switch into the relevant ports of the Arduino, as shown below. You will need several breadboard jumper cables.

![](images/attach_everything_together/electronics_diagram.svg)


## Stepper driver expansion to Arduino Uno

Connect the [42CH Stepper Motor Driver Expansion Board] and the [Arduino Uno] together as shown.

Connect the stepper driver expansion board ``DIR`` to Arduino ``Pin 2``.
![](images/assemble_base/wiring_1.jpg)

Connect the stepper driver expansion board ``STEP`` to Arduino ``Pin 3``.
![](images/assemble_base/wiring_2.jpg)

Connect the stepper driver expansion board ``EN`` to Arduino ``Pin 4``.
![](images/assemble_base/wiring_3.jpg)

Connect the stepper driver expansion board ``V`` to the Arduino ``5V Pin``.
![](images/assemble_base/wiring_4.jpg)
![](images/assemble_base/wiring_5.jpg)

Connect the stepper driver expansion board ``G`` to the Arduino Uno ``GND``.
![](images/assemble_base/wiring_6.jpg)

## DC socket to Stepper driver expansion

Connect the negative terminal of the [soldered DC socket](fromstep) to the stepper drive expansion board screw terminal ``GND``.
Connect the negative terminal of the [soldered DC socket](fromstep) to the stepper drive expansion board screw terminal ``VIN``.
![](images/assemble_base/fitting_dcsocket_3.jpg).

Before operation you should adjust the current limiting potentiometer on the [DRV8825] using [these instructions (under Current limiting)](https://www.pololu.com/product/2133).

Change the dip switches on the Stepper driver expansion board to:

| MS1 | MS2 | MS3 | Microstep Resolution|
|---|---|---|---|
|High|Low|Low|Half step|


## Button and swiches

Connect the positive terminal of the [soldered LED pushbutton](fromstep) button to the Arduino ``Pin 5``.
Connect the negative terminal of the LED (part of the [soldered LED pushbutton](fromstep)) to the stepper driver expansion board ``GND``.
Connect the positive terminal of the LED (part of the [soldered LED pushbutton](fromstep)) to the Arduino ``Pin 13``.
Connect the negative terminal of the [soldered LED pushbutton](fromstep) button to the Arduino ``GND``.
Connect the positive terminal of the [soldered limit switch](fromstep) to the Arduino ``Pin 6``.
Connect the negative terminal of the [soldered limit switch](fromstep) to the Arduino Pin ``GND``.

![](images/attach_everything_together/wiring_3.jpg)
![](images/attach_everything_together/wiring_4.jpg)
![](images/attach_everything_together/wiring_5.jpg)
![](images/attach_everything_together/wiring_6.jpg)

## Screw the lid onto the base {pagestep}

Push four [M3 nut]{qty:4}s into the base nut traps. Use four [M3x8mm screws]{qty:4} on the four corners to secure the [assembled lid](fromstep){qty:1} onto the [assembled base](fromstep){qty:1} with a [M2.5 hex screwdriver].

![](images/attach_everything_together/side_nut.jpg)
![](images/attach_everything_together/mount_lid.jpg)

## Attach the slider {pagestep}

Screw the [lead screw nut]{qty:1} onto the rotating arm of the nema motor.

![](images/attach_everything_together/mount_lead_nut.jpg)

Align the holes of the [assembled slider](fromstep){qty:1}  with two holes opposite each other of the [lead screw nut]{qty:1} and attach them together using two [M3x8mm screws]{qty:2} with a [M2.5 hex screwdriver].

![](images/attach_everything_together/mount_slider.jpg)
![](images/attach_everything_together/final_overview.jpg)

## Upload the Arduino code {pagestep}

Install the Arduino programming software (from the [website](https://www.arduino.cc/en/software)), along with the [Arduino code](https://gitlab.com/autohaem/autohaem-smear-plus-arduino) required for running this device. Upon downloading the software, open up the code. You will need to install the [`AccelStepper` library](https://www.arduino.cc/reference/en/libraries/accelstepper/).

![](images/attach_everything_together/arduino_download.jpg)

Connect the Arduino to the computer and select the corresponding port connected to the Arduino in the software (under ``Port`` in ``Tools``).

(The Arduino software version used at the time of this documentation Arduino IDE 1.8.15)

![](images/attach_everything_together/computer_port.jpg)
![](images/attach_everything_together/tools_port_screenshot.jpg)

At the top of the code, under ``//Adjust these variables``, are various parameters that can be altered:

* `reverseDistance`: The distance (in cm) moved back slowly from the end of the slide to the point where smearing begins
* `bloodSmearWidth`: The distance (in cm) travelled further back slowly during the blood smear
* `reverseSpeed`: The speed (in cm/s) at which the arm slowly moves back.
* `forwardSpeed`: The speed (in cm/s) at which the arm quickly moves forward.


![](images/attach_everything_together/parameters_screenshot.jpg)

When the appropriate parameters are adjusted, upload the code onto the Arduino of the autohaem smear+ device. 

## Plug in the power supplies {pagestep}

Connect the [Arduino power supply]{qty:1} to the right-most port (smaller rectangular hole) of the autohaem smear+ device. This will be used to power the Arduino Uno.

![](images/attach_everything_together/arduino_port.jpg)

Next, connect the [Motor power supply]{qty:1} to the circular port of the autohaem smear+ device. This will be used to power the motor.

![](images/attach_everything_together/motor_port.jpg)

# Nema 17 motor with leadscrew

Commonly used for 3D printers and CNC machines.  The stepper motor needs to come with an 100mm integrated Tr8x8 leadscrew.  It can be longer, but be careful if you are cutting down to size that the nut will still thread onto the end. 
## Motor

* Nema 17 motor
* Integrated 100mm Tr8x8 lead screw
* Step angle 1.8degree
* Rated voltage 3.3V DC
* Rated current 1.5A
* 

Available from e.g. [Amazon Iverntech AW0004](https://www.amazon.co.uk/gp/product/B08CHJW7GK/)

## Cable

Included with the above product

## Leadscrew nut

Tr8x8 leadscrew nut.  Included with the above product.
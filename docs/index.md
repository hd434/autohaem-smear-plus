# autohaem smear+

autohaem smear+ is a electro-mechanical device for producing automated blood smears.  

![](images/index/autohaem_smear_plus.jpg)

## Assembly

The [bill of materials]{BOM} can also be downloaded as an CSV.

The assembly instructions are divided into 5 sections:

1. [Prepare electronics](prepare_electronics.md){step}
2. [Assemble base](assemble_base.md){step}  
3. [Assemble lid](assemble_lid.md){step}
4. [Assemble slider](assemble_slider.md){step}
5. [Attach everything together](attach_everything_together.md){step}

## Operation

1. [Operating the device](operation.md){step}
